#GNSS
PRODUCT_PACKAGES += \
    gps.conf \
    gps.msm8974 \
    libloc_core \
    libloc_eng \
    libgps.utils \
    libloc_ds_api \
    libloc_api_v02
