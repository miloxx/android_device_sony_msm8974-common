# Copyright (C) 2014 The CyanogenMod Project
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

PLATFORM_PATH := device/sony/msm8974-common

BOARD_VENDOR := sony

# Use Snapdragon LLVM Compiler if available
TARGET_USE_SDCLANG := true
#untested
#TARGET_KERNEL_CLANG_COMPILE := true

# Include path
TARGET_SPECIFIC_HEADER_PATH += $(PLATFORM_PATH)/include


TARGET_NO_BOOTLOADER := true
TARGET_NO_RADIOIMAGE := true

# use CAF variants
BOARD_USES_QCOM_HARDWARE := true
TARGET_USES_QCOM_BSP := true
TARGET_QCOM_MEDIA_VARIANT := caf

# Platform
TARGET_BOOTLOADER_BOARD_NAME := MSM8974
TARGET_BOARD_PLATFORM := msm8974

# Architecture
TARGET_ARCH := arm
TARGET_CPU_ABI := armeabi-v7a
TARGET_CPU_ABI2 := armeabi
TARGET_ARCH_VARIANT := armv7-a-neon
TARGET_CPU_VARIANT := krait
TARGET_CUSTOM_DTBTOOL := dtbToolLineage
BOARD_KERNEL_IMAGE_NAME := zImage

MALLOC_SVELTE := true

#kernel
TARGET_KERNEL_SOURCE := kernel/sony/msm8974

# Kernel information
BOARD_KERNEL_BASE     := 0x00000000
BOARD_KERNEL_PAGESIZE := 2048
BOARD_MKBOOTIMG_ARGS  := --ramdisk_offset 0x02000000 --tags_offset 0x01E00000
BOARD_FLASH_BLOCK_SIZE := 131072 # (BOARD_KERNEL_PAGESIZE * 64)

# common cmdline parameters
#BOARD_KERNEL_CMDLINE += androidboot.selinux=permissive
BOARD_KERNEL_CMDLINE += msm_rtb.filter=0x3b7 ehci-hcd.park=3
BOARD_KERNEL_CMDLINE += dwc3.maximum_speed=high dwc3_msm.prop_chg_detect=Y
BOARD_KERNEL_CMDLINE += coherent_pool=8M vmalloc=300M
# Required for the 3.4 CAF kernel
BOARD_KERNEL_CMDLINE += androidboot.hardware=qcom
BOARD_KERNEL_CMDLINE += androidboot.wificountrycode=DE

# Defines for HW subsystems
-include $(PLATFORM_PATH)/hardware/*/BoardConfig.mk

# Audio
BOARD_USES_ALSA_AUDIO := true
AUDIO_FEATURE_DISABLED_USBAUDIO := true
AUDIO_FEATURE_ENABLED_EXTN_POST_PROC := true
AUDIO_FEATURE_ENABLED_MULTI_VOICE_SESSIONS := true

# Binder API version
TARGET_USES_64_BIT_BINDER := true

# Bluetooth
BOARD_HAVE_BLUETOOTH := true
N_BRCM_HCI := 26

# Camera
TARGET_PROVIDES_CAMERA_HAL := true
USE_DEVICE_SPECIFIC_CAMERA := true
TARGET_NEEDS_PLATFORM_TEXT_RELOCATIONS := true
TARGET_HAS_LEGACY_CAMERA_HAL1 := true
TARGET_NEEDS_LEGACY_CAMERA_HAL1_DYN_NATIVE_HANDLE := true

# Shims
TARGET_LD_SHIM_LIBS := \
    /system/vendor/bin/credmgrd|/system/vendor/lib/libshims_signal.so \
    /system/vendor/bin/iddd|/system/vendor/lib/libshims_idd.so \
    /system/vendor/bin/suntrold|/system/vendor/lib/libshims_signal.so \
    /system/lib/hw/camera.vendor.qcom.so|/system/vendor/lib/libsonycamera.so \
    /system/vendor/bin/mm-qcamera-daemon|libandroid.so \
    /system/lib/libcammw.so|libsensor.so \
    /system/lib/libsomc_chokoballpal.so|/system/vendor/lib/libshim_camera.so \
    /system/lib/libcald_pal.so|/system/vendor/lib/libshim_cald.so \
    /system/lib/hw/camera.vendor.qcom.so|libsensor.so

# Charger
HEALTHD_ENABLE_TRICOLOR_LED := true
BOARD_CHARGER_ENABLE_SUSPEND := true
BOARD_CHARGER_SHOW_PERCENTAGE := true
BOARD_CHARGER_DISABLE_INIT_BLANK := true
BOARD_GLOBAL_CFLAGS += -DBATTERY_REAL_INFO
BACKLIGHT_PATH :=/sys/class/leds/lcd-backlight/brightness
RED_LED_PATH := /sys/class/leds/red/brightness
GREEN_LED_PATH := /sys/class/leds/green/brightness
BLUE_LED_PATH := /sys/class/leds/blue/brightness

#FM
BOARD_HAVE_QCOM_FM := true

# Font
EXTENDED_FONT_FOOTPRINT := true

# exFAT
TARGET_EXFAT_DRIVER := exfat

# Graphics
USE_OPENGL_RENDERER := true
TARGET_USES_ION := true
NUM_FRAMEBUFFER_SURFACE_BUFFERS := 3
OVERRIDE_RS_DRIVER := libRSDriver_adreno.so
TARGET_ADDITIONAL_GRALLOC_10_USAGE_BITS := 0x02000000U
SF_VSYNC_EVENT_PHASE_OFFSET_NS := 5000000
VSYNC_EVENT_PHASE_OFFSET_NS := 7500000
TARGET_USES_GRALLOC1_ADAPTER := true

# Shader cache config options
# Maximum size of the  GLES Shaders that can be cached for reuse.
# Increase the size if shaders of size greater than 12KB are used.
MAX_EGL_CACHE_KEY_SIZE := 12*1024

# Maximum GLES shader cache size for each app to store the compiled shader
# binaries. Decrease the size if RAM or Flash Storage size is a limitation
# of the device.
MAX_EGL_CACHE_SIZE := 2048*1024

# Init configuration for init_sony
include $(PLATFORM_PATH)/common/init/config.mk
BOARD_USES_INIT_SONY := true

# Lights HAL
TARGET_PROVIDES_LIBLIGHT := true

# Power
TARGET_HAS_LEGACY_POWER_STATS := true
TARGET_HAS_NO_WLAN_STATS := true
TARGET_USES_INTERACTION_BOOST := true

# Keymaster
TARGET_PROVIDES_KEYMASTER := true

# RIL
#TARGET_RIL_VARIANT := caf

# SELinux
include device/qcom/sepolicy/sepolicy.mk
BOARD_SEPOLICY_DIRS += $(PLATFORM_PATH)/sepolicy
#PRODUCT_PRECOMPILED_SEPOLICY := false

# Treble
#PRODUCT_FULL_TREBLE := true
DEVICE_MANIFEST_FILE := $(PLATFORM_PATH)/treble-manifest.xml

# Enable dex-preoptimization to speed up first boot sequence
ifeq ($(HOST_OS),linux)
  ifneq ($(TARGET_BUILD_VARIANT),eng)
    ifeq ($(WITH_DEXPREOPT),)
      WITH_DEXPREOPT := true
      WITH_DEXPREOPT_BOOT_IMG_AND_SYSTEM_SERVER_ONLY := true
    endif
  endif
endif

DONT_DEXPREOPT_PREBUILTS := true
